## shapeShift: Pin-Based Phosphene Interface for Retinal Implants

The Pin-Based Phosphene Interface is a pioneering device designed to improve the visual experience of patients with retinal diseases leading to vision loss. This innovative technology complements retinal implants, often referred to as "Bionic Eyes," which aim to restore vision by electrically stimulating the retina to create phosphenes. Phosphenes are perceived points of light that, when combined, form a visual image.

### Key Features

1. **Enhanced Image Resolution:** The device focuses on enhancing the resolution of images generated by retinal implants by precisely controlling the stimulation of electrodes.

2. **Pin-to-Image Conversion:** It addresses the challenge of accurately mapping the phosphene shapes produced by stimulated electrodes, including variations between electrodes within the same array and among different individuals.

3. **Interactive Feedback:** The interface provides users with a tangible and interactive means to draw and manipulate phosphenes, creating a unique link between user input and visual perception.

4. **Cost-Effective and Scalable:** Our solution is designed to be cost-effective and easily scalable, making it accessible for a wide range of patients.

5. **Efficient Editing:** Users can edit inputs without resetting previous data, saving time and allowing for a more personalized experience.

6. **Consistency and Speed:** The device ensures fast and consistent inputs, improving the overall user experience.

### How It Works

The Pin-Based Phosphene Interface features a 3x3 array of pins that users can depress to control phosphenes' brightness. This pin depression mechanism incorporates a rack and pinion concept inspired by steering mechanisms in cars. As users depress the pins, a gear linked to a dial rotates, altering the voltage within the circuit. Spring mechanisms reset the pins to their initial positions when the pin's button is pressed, disengaging the ratchet using a servo motor.

![Alt text](<Device overview resized.jpg>)
![Alt text](<button ratchet.png>)

The changes in voltage within the circuit are recorded by an Arduino processor, which transmits the data to MATLAB for image conversion. Each pin's height is calibrated to its corresponding voltage to ensure the desired output.

### Future Potential

The prototype design is modular and scalable, offering the potential for constructing larger devices with sufficient funding. A larger device promises higher-resolution images and an improved visual experience for users.

### Enhancements

For future iterations, the device can benefit from the use of stronger materials, smaller sensors, and an array with a hexagonal pin arrangement, which would reduce the distance between pins and create more cohesive images.
