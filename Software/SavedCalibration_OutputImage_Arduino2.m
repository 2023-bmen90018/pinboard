figure(1); clf

clear
clc

%% General Arduino setup
if ~exist('a','var')
    a = arduino();
end

%% Image setup
pins = 9;
sets = [0 0 0;...
    1 0 0;...
    0 1 0;...
    1 1 0;...
    0 0 1;...
    1 0 1;...
    0 1 1;...
    1 1 1];
v = zeros(sqrt(pins));

%% Initialisation
if exist('CalibrationSettings.mat','file')
    load('CalibrationSettings.mat')
else
    init = zeros(sqrt(pins));
    bottom = init;

    disp('Raise all pins to maximum height, then press Enter to continue')
    waitforbuttonpress

    for pin = 1:pins
        if pin ~= 9
            writeDigitalPin(a,'D47',sets(pin,1));
            writeDigitalPin(a,'D51',sets(pin,2));
            writeDigitalPin(a,'D53',sets(pin,3));

            pause(0.1)
            init(pin) = readVoltage(a,'A1');
        else
            init(pin) = readVoltage(a,'A3');
        end
    end

    disp('Lower all pins to minimum height, then press Enter to continue')
    waitforbuttonpress

    for pin = 1:pins
        if pin ~= 9
            writeDigitalPin(a,'D47',sets(pin,1));
            writeDigitalPin(a,'D51',sets(pin,2));
            writeDigitalPin(a,'D53',sets(pin,3));

            pause(0.1)
            bottom(pin) = readVoltage(a,'A1');
        else
            bottom(pin) = readVoltage(a,'A3');
        end
    end
    save('CalibrationSettings.mat','init','bottom');
end

%% Loop
phosphene = zeros(3);
array = zeros(5);
while true
    for pin = 1:pins
        %% Image output in loop
        if pin ~= 9
            writeDigitalPin(a,'D47',sets(pin,1));
            writeDigitalPin(a,'D51',sets(pin,2));
            writeDigitalPin(a,'D53',sets(pin,3));

            v(pin) = readVoltage(a,'A1');
            if (init(pin)-v(pin)) >= 0
                phosphene(pin) = (init(pin)-v(pin))/(init(pin)-bottom(pin))*5;
                if phosphene(pin) > 5
                    phosphene(pin) = 5;
                end
            else
                phosphene(pin) = 0;
            end
        else
            v(pin) = readVoltage(a,'A3');
            if (init(pin)-v(pin)) >= 0
                phosphene(pin) = (init(pin)-v(pin))/(init(pin)-bottom(pin))*5;
                if phosphene(pin) > 5
                    phosphene(pin) = 5;
                end
            else
                phosphene(pin) = 0;
            end
        end
    end
    array(2:4,2:4) = phosphene;
    Vq = interp2(array,5,'makima');
    colormap gray
    
    imagesc(Vq)
    caxis([0 5])
    axis off
    axis square

    pause(0.01)
end
