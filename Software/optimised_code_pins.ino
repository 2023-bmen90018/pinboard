#include <Servo.h>

const int numServos = 9;  // Number of servos
Servo servos[numServos];   // Array to store servo instances
int servoPins[numServos] = {2, 3, 4, 5, 6, 7, 8, 9, 10};  // Servo pins

const int buttonPins[numServos] = {23, 25, 27, 29, 31, 33, 35, 37, 39};  // Button pins

// "Level" indicates different positions of the servo motors, each with a unique ratchet length
const int pos_lvl1 = 10;  // Position for level 1
const int pos_lvl2 = 10;  // Position for level 2
const int pos_lvl3 = 10;  // Position for level 3

void setup() {
  // Initialize servos and button pins
  for (int i = 0; i < numServos; i++) {
    servos[i].attach(servoPins[i]);
    pinMode(buttonPins[i], INPUT_PULLUP);  // Use internal pull-up resistors
  }
}

void loop() {
  for (int i = 0; i < numServos; i++) {
    int buttonState = digitalRead(buttonPins[i]);
    
    if (i < 3) {
      if (buttonState == 1) {
        servos[i].write(pos_lvl1);  // Move servo to level 1 position
      } else {
        servos[i].write(0);  // Reset servo position
      }
    } else if (i < 6) {
      if (buttonState == 1) {
        servos[i].write(pos_lvl2);  // Move servo to level 2 position
      } else {
        servos[i].write(0);  // Reset servo position
      }
    } else {
      if (buttonState == 1) {
        servos[i].write(pos_lvl3);  // Move servo to level 3 position
      } else {
        servos[i].write(0);  // Reset servo position
      }
    }
  }
}
