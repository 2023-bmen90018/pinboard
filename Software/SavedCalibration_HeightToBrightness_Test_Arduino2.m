figure(1); clf

clear
clc

%% General Arduino setup
if ~exist('a','var')
    a = arduino();
end

%% Image setup
pins = 9;
sets = [0 0 0;...
    1 0 0;...
    0 1 0;...
    1 1 0;...
    0 0 1;...
    1 0 1;...
    0 1 1;...
    1 1 1];
v = zeros(sqrt(pins));

%% Initialisation
if exist('CalibrationSettings.mat','file')
    load('CalibrationSettings.mat')
else
    init = zeros(sqrt(pins));
    bottom = init;

    disp('Raise all pins to maximum height, then press Enter to continue')
    waitforbuttonpress

    for pin = 1:pins
        if pin ~= 9
            writeDigitalPin(a,'D47',sets(pin,1));
            writeDigitalPin(a,'D51',sets(pin,2));
            writeDigitalPin(a,'D53',sets(pin,3));

            pause(0.1)
            init(pin) = readVoltage(a,'A1');
        else
            init(pin) = readVoltage(a,'A3');
        end
    end

    disp('Lower all pins to minimum height, then press Enter to continue')
    waitforbuttonpress

    for pin = 1:pins
        if pin ~= 9
            writeDigitalPin(a,'D47',sets(pin,1));
            writeDigitalPin(a,'D51',sets(pin,2));
            writeDigitalPin(a,'D53',sets(pin,3));

            pause(0.1)
            bottom(pin) = readVoltage(a,'A1');
        else
            bottom(pin) = readVoltage(a,'A3');
        end
    end
    save('CalibrationSettings.mat','init','bottom');
end

% %% Servo + button setup
% % Define the number of servos
% numServos = 9;
% 
% % Define servo positions for different levels
% pos_lvl1 = 5;
% pos_lvl2 = 7;
% pos_lvl3 = 10;
% 
% % Define servo pins
% servoPins = {'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10'};
% 
% % Define button pins
% buttonPins = {'D23', 'D25', 'D27', 'D29', 'D31', 'D33', 'D35', 'D37', 'D39'};
% 
% % Attach servos and configure button pins
% for i = 1:numServos
%     servoObj = servo(a, servoPins{i});
%     servos{i} = servoObj;
%     configurePin(a, buttonPins{i}, 'digitalInput');
% end
% 
% buttonState = ones(sqrt(numServos));

%% Loop
phosphene = zeros(3);
array = zeros(5);
while true
    for pin = 1:pins
%         %% Button and motor control in loop
%         buttonState(pin) = readDigitalPin(a, buttonPins{pin});
% %         buttonState
%         if pin <= 3
%             if buttonState(pin) == 1
%                 writePosition(servos{pin}, pos_lvl1/180); % Convert to normalized value
%             else
%                 writePosition(servos{pin}, 0);
%             end
%         elseif pin <= 6
%             if buttonState(pin) == 1
%                 writePosition(servos{pin}, pos_lvl2/180);
%             else
%                 writePosition(servos{pin}, 0);
%             end
%         else
%             if buttonState(pin) == 1
%                 writePosition(servos{pin}, pos_lvl3/180);
%             else
%                 writePosition(servos{pin}, 0);
%             end
%         end
        %% Image output in loop
        if pin ~= 9
            writeDigitalPin(a,'D47',sets(pin,1));
            writeDigitalPin(a,'D51',sets(pin,2));
            writeDigitalPin(a,'D53',sets(pin,3));

            v(pin) = readVoltage(a,'A1');
            if (init(pin)-v(pin)) >= 0
                phosphene(pin) = (init(pin)-v(pin))/(init(pin)-bottom(pin))*5;
                if phosphene(pin) > 5
                    phosphene(pin) = 5;
                end
            else
                phosphene(pin) = 0;
            end
        else
            v(pin) = readVoltage(a,'A3');
            if (init(pin)-v(pin)) >= 0
                phosphene(pin) = (init(pin)-v(pin))/(init(pin)-bottom(pin))*5;
                if phosphene(pin) > 5
                    phosphene(pin) = 5;
                end
            else
                phosphene(pin) = 0;
            end
        end
    end
    [v phosphene]
    array(2:4,2:4) = phosphene;
    Vq = interp2(array,5,'makima');
    colormap gray
    
    subplot(1,2,1)
    
    imagesc(Vq)
    caxis([0 5])
    axis off
    axis square

    subplot(1,2,2)
    scatter(1:9,20.*reshape(phosphene,[1 9]),'filled')
    hold on
    scatter(1:9, [100 100 100 50 50 50 0 0 0],'x')
    hold off
    axis([0 10 0 100])
    xlabel('Pin number')
    ylabel('Image intensity (% of max)')
    xticks(1:9)
    pause(0.01)
end
