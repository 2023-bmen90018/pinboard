% Images phosphenes as overlapping Gaussians. This version generalises 
% the size of the array, so that sizes other than 8x8 can be used. 
% Individual phosphenes are depicted as 2D Gaussians that are multiplied by 
% a calibration factor and by the most recently sampled voltage from the 
% corresponding pin. The image depicting these phosphenes are updated 1 
% column at a time, corresponding to the number of phosphenes that can be 
% sampled at once given the need for multiplexers.

close all
clear
clc

phosphene_number = 64; % should be a square number
gap_size = 9;
variance = 13; % note: increasing this increases the overlap between adjacent phosphenes, but can cause artifacts in the orthogonal directions from phosphene centers

phosphene_size = 2*gap_size+1;
phosphene_dimensions = sqrt(phosphene_number);
bound = gap_size;
mu = [0 0];
Sigma = [variance 0;0 variance];
x1 = -bound:1:bound; x2 = -bound:1:bound;
[X1,X2] = meshgrid(x1,x2);
F = mvnpdf([X1(:) X2(:)],mu,Sigma);
F = reshape(F,length(x2),length(x1));
F = 800.*F;

gaps = gap_size + 1;
array = zeros(phosphene_dimensions*gaps+gap_size);
last_pin = size(array,1)-gap_size;
array(gaps:gaps:last_pin,gaps:gaps:last_pin) = 1;
[xc,yc] = find(array);
array = zeros(phosphene_dimensions*gaps+gap_size);

pins = length(xc);

phosphene = zeros(phosphene_size,phosphene_size,pins);
time = 10;
while time > 0
    for pin = 1:pins
        array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)-phosphene(:,:,pin);
        phosphene(:,:,pin) = F.*rand();
        array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);
        
        if rem(pin,sqrt(pins))==0
            colormap gray
            imagesc(array)
            clim([0 5])
            axis off
            axis square
            pause(0.1)
        end
    end
    time = time - 1;
end
