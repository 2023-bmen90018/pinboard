% Takes reading from the output of a digital 8-to-1 multiplexer (74HC151)
% and presents the output voltage as a Gaussian image (to represent a
% phosphene)
% In this test, the pins of the multiplexer are connected as follows:
% Pin 1 (I3) = D4
% Pins 2-4 (I0-I2) = GND
% Pin 5 (Y) = A0
% Pin 7 (E_bar) = GND
% Pin 8 (GND) = GND
% Pin 9 (S2) = D7
% Pin 10 (S1) = D6
% Pin 11 (S0) = D5
% Pins 12-15 (I4-I7) = GND
% Pin 16 (VCC) = +5V

clc

if ~exist('a','var')
    a = arduino();
end

writePWMVoltage(a,'D4',3.9);

figure(1)
colormap gray
time = 100;
i = 1;
while time > 0
    voltage_high = 5;
    voltage_low = 0.1;
    for j = 1:10
        voltage1 = readVoltage(a, 'A0');
        image = voltage1 * gauss2d(zeros(25),5,[13,13]);

        colormap gray
        imagesc(image)
        caxis([1 2])
        axis off
        pause(0.1)
    end
    time = time - 0.1;
    
    switch i
        case 1
            writePWMVoltage(a,'D5',voltage_low);
            writePWMVoltage(a,'D6',voltage_low);
            writePWMVoltage(a,'D7',voltage_low);
        case 2
            writePWMVoltage(a,'D5',voltage_high);
            writePWMVoltage(a,'D6',voltage_low);
            writePWMVoltage(a,'D7',voltage_low);
        case 3
            writePWMVoltage(a,'D5',voltage_low);
            writePWMVoltage(a,'D6',voltage_high);
            writePWMVoltage(a,'D7',voltage_low);
        case 4
            writePWMVoltage(a,'D5',voltage_high);
            writePWMVoltage(a,'D6',voltage_high);
            writePWMVoltage(a,'D7',voltage_low);
        case 5
            writePWMVoltage(a,'D5',voltage_low);
            writePWMVoltage(a,'D6',voltage_low);
            writePWMVoltage(a,'D7',voltage_high);
        case 6
            writePWMVoltage(a,'D5',voltage_high);
            writePWMVoltage(a,'D6',voltage_low);
            writePWMVoltage(a,'D7',voltage_high);
        case 7
            writePWMVoltage(a,'D5',voltage_low);
            writePWMVoltage(a,'D6',voltage_high);
            writePWMVoltage(a,'D7',voltage_high);
        case 8
            writePWMVoltage(a,'D5',voltage_high);
            writePWMVoltage(a,'D6',voltage_high);
            writePWMVoltage(a,'D7',voltage_high);
    end
    
    if i < 8
        i = i + 1
    else
        i = 1
    end
end

function mat = gauss2d(mat, sigma, center)
    gsize = size(mat);
    for r=1:gsize(1)
        for c=1:gsize(2)
            mat(r,c) = gaussC(r,c, sigma, center);
        end
    end
end

function val = gaussC(x, y, sigma, center)
    xc = center(1);
    yc = center(2);
    exponent = ((x-xc).^2 + (y-yc).^2)./(2*sigma);
    val       = (exp(-exponent));
end