% Takes reading from the output of a digital 8-to-1 multiplexer (74HC151)
% and presents the output voltage at the corresponding position on a 2x4
% array:
%   1   3   5   7
%   2   4   6   8
%
% In this test, the pins of the multiplexer are connected to the pins of the Arduino Mega as follows:
% Pin 1 (I3) = D13
% Pin 2 (I2) = D12
% Pin 3 (I1) = D11
% Pin 4 (I0) = D10
% Pin 5 (Y) = A0
% Pin 6 (Y_bar) = no connection
% Pin 7 (E_bar) = GND
% Pin 8 (GND) = GND
% Pin 9 (S2) = D53
% Pin 10 (S1) = D51
% Pin 11 (S0) = D49
% Pin 12 (I7) = D6
% Pin 13 (I6) = D7
% Pin 14 (I5) = D8
% Pin 15 (I4) = D9
% Pin 16 (VCC) = +5V

% This code cycles through all 8 permutations of selection inputs in order
% to take inputs from each of the 8 input channels, one at a time. This can be
% changed by altering which pins have 0s and 1s in the writeDigitalPin functions on
% lines 36-43

clc

if ~exist('a','var')
    a = arduino();
end

writeDigitalPin(a,'D6',1);
writeDigitalPin(a,'D7',0);
writeDigitalPin(a,'D8',0);
writeDigitalPin(a,'D9',0);
writeDigitalPin(a,'D10',0);
writeDigitalPin(a,'D11',0);
writeDigitalPin(a,'D12',1);
writeDigitalPin(a,'D13',0);

image = zeros(1,8);

figure(1)
time = 100;
i = 1;
while time > 0
    for j = 1:5
        voltage1 = readVoltage(a, 'A0');
        
        image(i) = voltage1;
        image_shifted = circshift(image,-1);
        image_2d = reshape(image_shifted,[2,4]);
        imagesc(image_2d)
        colormap gray
        colorbar
        caxis([0 5])
        axis off
        pause(0.1)
    end
    time = time - 0.1;
    
    switch i
        case 1
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',0);
        case 2
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',0);
        case 3
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',0);
        case 4
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',0);
        case 5
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',1);
        case 6
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',1);
        case 7
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',1);
        case 8
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',1);
    end
    
    if i < 8
        i = i + 1
    else
        i = 1
    end
end

% function mat = gauss2d(mat, sigma, center)
%     gsize = size(mat);
%     for r=1:gsize(1)
%         for c=1:gsize(2)
%             mat(r,c) = gaussC(r,c, sigma, center);
%         end
%     end
% end
% 
% function val = gaussC(x, y, sigma, center)
%     xc = center(1);
%     yc = center(2);
%     exponent = ((x-xc).^2 + (y-yc).^2)./(2*sigma);
%     val       = (exp(-exponent));
% end