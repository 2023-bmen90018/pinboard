% Takes reading from the output of a analog 8-to-1 multiplexer (74HC4051N)
% and presents the output voltage at the corresponding position on a 2x4
% array:
%   1   3   5   7
%   2   4   6   8
%
% In this test, the pins of the multiplexer are connected to the pins of the Arduino Mega as follows:
% Pin 1 (Y4) = D13
% Pin 2 (Y6) = D12
% Pin 3 (Z) = A0
% Pin 4 (I7) = D11
% Pin 5 (Y5) = D10
% Pin 6 (E_bar) = GND
% Pin 7 (V_EE) = GND
% Pin 8 (GND) = GND
% Pin 9 (S2) = D53
% Pin 10 (S1) = D51
% Pin 11 (S0) = D49
% Pin 12 (Y3) = D6
% Pin 13 (Y0) = D7
% Pin 14 (Y1) = D8
% Pin 15 (Y2) = D9
% Pin 16 (VCC) = +5V
% Therefore, the output image has the following connections:
%   D7  D9  D13  D12
%   D8  D6  D10  D11

% This code cycles through all 8 permutations of selection inputs in order
% to take inputs from each of the 8 input channels, one at a time. Since 
% the writePWMVoltage function doesn't work in this case, this must be
% done manually by using voltage division

clc

if ~exist('a','var')
    a = arduino();
end

% % Configure pins to PWM
% pinnames = {'D7','D8','D9','D6','D13','D10','D12','D11'};
% for j = 1:length(pinnames)
%     configurePin(a,string(pinnames(j)),'PWM')
% end
% % Position 1 (Y0)
% writePWMVoltage(a,'D7',1);
% writePWMDutyCycle(a,'D7',1);
% % Position 2 (Y1)
% writePWMVoltage(a,'D8',0);
% writePWMDutyCycle(a,'D8',0);
% % Position 3 (Y2)
% writePWMVoltage(a,'D9',0);
% writePWMDutyCycle(a,'D9',0);
% % Position 4 (Y3)
% writePWMVoltage(a,'D6',0);
% writePWMDutyCycle(a,'D6',0);
% % Position 5 (Y4)
% writePWMVoltage(a,'D13',0);
% writePWMDutyCycle(a,'D13',0);
% % Position 6 (Y5)
% writePWMVoltage(a,'D10',0);
% writePWMDutyCycle(a,'D10',0);
% % Position 7 (Y6)
% writePWMVoltage(a,'D12',5);
% writePWMDutyCycle(a,'D12',1);
% % Position 8 (Y7)
% writePWMVoltage(a,'D11',0);
% writePWMDutyCycle(a,'D11',0);

image = zeros(1,8);

figure(1)
time = 2;
i = 1;
while time > 0
    for j = 1:10
        voltage1 = readVoltage(a, 'A0');
        
        image(i) = voltage1;
        image_shifted = circshift(image,-1);
        image_2d = reshape(image_shifted,[2,4]);
        imagesc(image_2d)
        colormap gray
        colorbar
        caxis([0 5])
        axis off
        pause(0.1)
    end
    time = time - 0.1;
    
    switch i
        case 1
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',0);
        case 2
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',0);
        case 3
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',0);
        case 4
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',0);
        case 5
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',1);
        case 6
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',0);
            writeDigitalPin(a,'D53',1);
        case 7
            writeDigitalPin(a,'D49',0);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',1);
        case 8
            writeDigitalPin(a,'D49',1);
            writeDigitalPin(a,'D51',1);
            writeDigitalPin(a,'D53',1);
    end
    
    if i < 8
        i = i + 1
    else
        i = 1
    end
end

% function mat = gauss2d(mat, sigma, center)
%     gsize = size(mat);
%     for r=1:gsize(1)
%         for c=1:gsize(2)
%             mat(r,c) = gaussC(r,c, sigma, center);
%         end
%     end
% end
% 
% function val = gaussC(x, y, sigma, center)
%     xc = center(1);
%     yc = center(2);
%     exponent = ((x-xc).^2 + (y-yc).^2)./(2*sigma);
%     val       = (exp(-exponent));
% end