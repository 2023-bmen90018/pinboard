% Images phosphenes as 3x3 overlapping Gaussians. 
% This version integrates the multiplexer's function: only 1 column can be
% sampled at a time due to the limited number of analog pins in even the
% Arduino Mega. So, the code for rotating between selection inputs and 
% reading from the analog input channels is
% copied from MultiplexerOutput_V5 to achieve this.

close all
clear
clc

if ~exist('a','var')
    a = arduino();
end

phosphene_number = 9; % for GaussianImage_Multiplexer_V2.m, this value must be kept at 64; however, the result if you change it is pretty funny
gap_size = 9;
variance = 13; % note: increasing this increases the overlap between adjacent phosphenes, but can cause artifacts in the orthogonal directions from phosphene centers

phosphene_size = 2*gap_size+1;
phosphene_dimensions = sqrt(phosphene_number);
bound = gap_size;
mu = [0 0];
Sigma = [variance 0;0 variance];
x1 = -bound:1:bound; x2 = -bound:1:bound;
[X1,X2] = meshgrid(x1,x2);
F = mvnpdf([X1(:) X2(:)],mu,Sigma);
F = reshape(F,length(x2),length(x1));
F = 800.*F;

gaps = gap_size + 1;
array = zeros(phosphene_dimensions*gaps+gap_size);
last_pin = size(array,1)-gap_size;
array(gaps:gaps:last_pin,gaps:gaps:last_pin) = 1;
[xc,yc] = find(array);
array = zeros(phosphene_dimensions*gaps+gap_size);

pins = length(xc);

phosphene = zeros(phosphene_size,phosphene_size,pins);
time = 100;
i = 1;
while time > 0
    for pin = 1:pins
        pin
        array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)-phosphene(:,:,pin);
        switch pin
            case 1
                writeDigitalPin(a,'D49',0);
                writeDigitalPin(a,'D51',0);
                writeDigitalPin(a,'D53',0);
                pause(0.2)
                phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);

            case 2
                writeDigitalPin(a,'D49',1);
                writeDigitalPin(a,'D51',0);
                writeDigitalPin(a,'D53',0);
                pause(0.2)
                phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);

            case 3
                writeDigitalPin(a,'D49',0);
                writeDigitalPin(a,'D51',1);
                writeDigitalPin(a,'D53',0);
                pause(0.2)
                phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);

            case 4
                writeDigitalPin(a,'D49',1);
                writeDigitalPin(a,'D51',1);
                writeDigitalPin(a,'D53',0);
                pause(0.2)
                phosphene(:,:,pin) = 10*F*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);
            case 5
                phosphene(:,:,pin) = F.*readVoltage(a, 'A1');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);
                pause(0.2)
            case 6
                writeDigitalPin(a,'D49',0);
                writeDigitalPin(a,'D51',0);
                writeDigitalPin(a,'D53',1);
                pause(0.1)
                phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);

            case 7
                writeDigitalPin(a,'D49',1);
                writeDigitalPin(a,'D51',0);
                writeDigitalPin(a,'D53',1);
                pause(0.2)
                phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);

            case 8
                writeDigitalPin(a,'D49',0);
                writeDigitalPin(a,'D51',1);
                writeDigitalPin(a,'D53',1);
                pause(0.2)
                phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);

            case 9
                writeDigitalPin(a,'D49',1);
                writeDigitalPin(a,'D51',1);
                writeDigitalPin(a,'D53',1);
                pause(0.2)
                phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);

        end        
        
        colormap gray
%             array(ceil(gaps/2):gaps:end,:) = 1;
%             array(:,ceil(gaps/2):gaps:end) = 1;
%             imagesc(5-array)
        array(ceil(gaps/2):gaps:end,:) = 0;
        array(:,ceil(gaps/2):gaps:end) = 0;
        imagesc(array)
        caxis([0 50])
        axis off
        axis square
    end
    time = time - 1;
end
