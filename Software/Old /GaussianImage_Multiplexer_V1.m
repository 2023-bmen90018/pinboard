% Images phosphenes as 8x8 overlapping Gaussians. 
% This version integrates the multiplexer's function: only 1 column can be
% sampled at a time due to the limited number of analog pins in even the
% Arduino Mega. So, the code for rotating between selection inputs is
% copied from MultiplexerOutput_V5 to achieve this.
% However, this version replaces the Arduino functionality with calling
% random values so that it can be tested without an Arduino.
% The code that uses Arduino functionality can be seen in comments.

close all
clear
clc

phosphene_number = 64; % for GaussianImage_Multiplexer_V1.m, this value must be kept at 64; however, the result if you change it is pretty funny
gap_size = 9;
variance = 13; % note: increasing this increases the overlap between adjacent phosphenes, but can cause artifacts in the orthogonal directions from phosphene centers

phosphene_size = 2*gap_size+1;
phosphene_dimensions = sqrt(phosphene_number);
bound = gap_size;
mu = [0 0];
Sigma = [variance 0;0 variance];
x1 = -bound:1:bound; x2 = -bound:1:bound;
[X1,X2] = meshgrid(x1,x2);
F = mvnpdf([X1(:) X2(:)],mu,Sigma);
F = reshape(F,length(x2),length(x1));
F = 800.*F;

gaps = gap_size + 1;
array = zeros(phosphene_dimensions*gaps+gap_size);
last_pin = size(array,1)-gap_size;
array(gaps:gaps:last_pin,gaps:gaps:last_pin) = 1;
[xc,yc] = find(array);
array = zeros(phosphene_dimensions*gaps+gap_size);

pins = length(xc);

voltage_values = zeros(phosphene_dimensions);
voltage_values(3,5) = 2;
voltage_values(1,8) = 0.5;

phosphene = zeros(phosphene_size,phosphene_size,pins);
time = 10;
i = 1;
while time > 0
    for pin = 1:pins
        array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)-phosphene(:,:,pin);
        row = rem(pin,sqrt(pins));
        switch row
            case 1
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A0');
                phosphene(:,:,pin) = F.*voltage_values(row,i);
            case 2
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A1');
                phosphene(:,:,pin) = F.*voltage_values(row,i);
            case 3
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A2');
                phosphene(:,:,pin) = F.*voltage_values(row,i);
            case 4
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A3');
                phosphene(:,:,pin) = F.*voltage_values(row,i);
            case 5 
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A4');
                phosphene(:,:,pin) = F.*voltage_values(row,i);
            case 6
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A5');
                phosphene(:,:,pin) = F.*voltage_values(row,i);
            case 7
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A6');
                phosphene(:,:,pin) = F.*voltage_values(row,i);
            case 0
%                 phosphene(:,:,pin) = F.*readVoltage(a, 'A7');
                phosphene(:,:,pin) = F.*voltage_values(8,i);
        end
        array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)=array(xc(pin)-bound:1:xc(pin)+bound,yc(pin)-bound:1:yc(pin)+bound)+phosphene(:,:,pin);
        
        if rem(pin,sqrt(pins))==0
            colormap gray
            imagesc(array)
            clim([0 5])
            axis off
            axis square
            pause(0.1)
            if i < 8
                i = i + 1
            else
                i = 1
            end
%             switch i
%                 case 1
%                     writeDigitalPin(a,'D49',0);
%                     writeDigitalPin(a,'D51',0);
%                     writeDigitalPin(a,'D53',0);
%                 case 2
%                     writeDigitalPin(a,'D49',1);
%                     writeDigitalPin(a,'D51',0);
%                     writeDigitalPin(a,'D53',0);
%                 case 3
%                     writeDigitalPin(a,'D49',0);
%                     writeDigitalPin(a,'D51',1);
%                     writeDigitalPin(a,'D53',0);
%                 case 4
%                     writeDigitalPin(a,'D49',1);
%                     writeDigitalPin(a,'D51',1);
%                     writeDigitalPin(a,'D53',0);
%                 case 5
%                     writeDigitalPin(a,'D49',0);
%                     writeDigitalPin(a,'D51',0);
%                     writeDigitalPin(a,'D53',1);
%                 case 6
%                     writeDigitalPin(a,'D49',1);
%                     writeDigitalPin(a,'D51',0);
%                     writeDigitalPin(a,'D53',1);
%                 case 7
%                     writeDigitalPin(a,'D49',0);
%                     writeDigitalPin(a,'D51',1);
%                     writeDigitalPin(a,'D53',1);
%                 case 8
%                     writeDigitalPin(a,'D49',1);
%                     writeDigitalPin(a,'D51',1);
%                     writeDigitalPin(a,'D53',1);
%             end
        end
    end
    time = time - 1;
end
