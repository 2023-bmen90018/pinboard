% Takes reading from the output of a digital 8-to-1 multiplexer (74HC151)
% and presents the output voltage at the corresponding position on a 2x4
% array:
%   1   3   5   7
%   2   4   6   8
%
% In this test, the pins of the multiplexer are connected as follows:
% Pin 1 (I3) = D4
% Pins 2-4 (I0-I2) = GND
% Pin 5 (Y) = A0
% Pin 7 (E_bar) = GND
% Pin 8 (GND) = GND
% Pin 9 (S2) = D7
% Pin 10 (S1) = D6
% Pin 11 (S0) = D5
% Pins 12-15 (I4-I7) = GND
% Pin 16 (VCC) = +5V

% This code cycles through all 8 permutations of selection inputs in order
% to take inputs from each of the 8 input channels, one at a time. However,
% for this setup, only I3 should have a non-zero voltage

clc

if ~exist('a','var')
    a = arduino();
end

writeDigitalPin(a,'D4',1);

image = zeros(2,4);

figure(1)
time = 100;
i = 1;
while time > 0
    voltage_high = 5;
    voltage_low = 0.1;
    for j = 1:20
        voltage1 = readVoltage(a, 'A0');
        
        image(i) = voltage1;
        imagesc(image)
        colormap gray
        colorbar
        caxis([0 5])
        axis off
        pause(0.1)
    end
    time = time - 0.1;
    
    switch i
        case 1
            writeDigitalPin(a,'D5',0);
            writeDigitalPin(a,'D6',0);
            writeDigitalPin(a,'D7',0);
        case 2
            writeDigitalPin(a,'D5',1);
            writeDigitalPin(a,'D6',0);
            writeDigitalPin(a,'D7',0);
        case 3
            writeDigitalPin(a,'D5',0);
            writeDigitalPin(a,'D6',1);
            writeDigitalPin(a,'D7',0);
        case 4
            writeDigitalPin(a,'D5',1);
            writeDigitalPin(a,'D6',1);
            writeDigitalPin(a,'D7',0);
        case 5
            writeDigitalPin(a,'D5',0);
            writeDigitalPin(a,'D6',0);
            writeDigitalPin(a,'D7',1);
        case 6
            writeDigitalPin(a,'D5',1);
            writeDigitalPin(a,'D6',0);
            writeDigitalPin(a,'D7',1);
        case 7
            writeDigitalPin(a,'D5',0);
            writeDigitalPin(a,'D6',1);
            writeDigitalPin(a,'D7',1);
        case 8
            writeDigitalPin(a,'D5',1);
            writeDigitalPin(a,'D6',1);
            writeDigitalPin(a,'D7',1);
    end
    
    if i < 8
        i = i + 1
    else
        i = 1
    end
end

% function mat = gauss2d(mat, sigma, center)
%     gsize = size(mat);
%     for r=1:gsize(1)
%         for c=1:gsize(2)
%             mat(r,c) = gaussC(r,c, sigma, center);
%         end
%     end
% end
% 
% function val = gaussC(x, y, sigma, center)
%     xc = center(1);
%     yc = center(2);
%     exponent = ((x-xc).^2 + (y-yc).^2)./(2*sigma);
%     val       = (exp(-exponent));
% end